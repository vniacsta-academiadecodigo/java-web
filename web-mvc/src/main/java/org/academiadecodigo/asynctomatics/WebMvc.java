package org.academiadecodigo.asynctomatics;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class WebMvc extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User user = new User();
        user.setName("Vania Costa");
        user.setPhone(919191919);
        user.setEmail("hello@vniacsta.com");

        req.setAttribute("user", user);

        RequestDispatcher myJavabank = getServletContext().getRequestDispatcher("/WEB-INF/index.jsp");
        myJavabank.forward(req, resp);
    }

}
