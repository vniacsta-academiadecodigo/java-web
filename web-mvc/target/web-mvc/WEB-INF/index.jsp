<html>
<head>
    <title>Vania's Javabank</title>
    <style>
        body {
            text-align: center;
            background: darkslategrey;
            color: #fff;
            font-family: Cambria, serif;
        }

        div {
            width: fit-content;
            padding: 10px 30px;
            margin: 50px auto;
            border: solid 0.5px #fff;
            border-radius: 5px;
        }

        .input {
            display: block;
            margin: 10px;
            padding: 10px 20px;
            border: solid 0.5px #fff;
            border-radius: 5px;
            width: 250px;
        }

        .submit {
            width: 100px;
        }

    </style>
</head>
<body>
<div>
    <%--        <h1>JAVABANK - Customer #1</h1>--%>
    <%--        <hr>--%>
    <%--        <h2>Vania Costa</h2>--%>
    <%--        <p>Phone: 99191919</p>--%>
    <%--        <p>Email: hello@vniacsta.com</p>--%>
    <h1>JAVABANK - Customer #1</h1>
    <hr>
    <h2>${user.name}</h2>
    <p>Phone: ${user.phone}</p>
    <p>Email: ${user.email}</p>
</div>

<div class="form">
    <h3>Contact us:</h3>
    <form action="/web-mvc/" method="post">
        <input
                class="input"
                type="text"
                name="name"
                placeholder="Name"
                maxlength="50"
                required
        />
        <input
                class="input"
                type="email"
                name="email"
                autocomplete="email"
                placeholder="Email"
                maxlength="50"
                required
        />
        <textarea
                class="input"
                name="text"
                placeholder="Message"
                maxlength="1000"
                required
        ></textarea>
        <input
                class="input submit"
                type="submit"
                value="Submit"
        />
    </form>
</div>
</body>
</html>
