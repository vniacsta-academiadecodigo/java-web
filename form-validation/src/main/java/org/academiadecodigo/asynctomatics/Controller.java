package org.academiadecodigo.asynctomatics;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Controller extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("/form-validation/index.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        if (username == null || username.isEmpty() || email == null || email.isEmpty() || password == null || password.isEmpty()) {
            resp.sendRedirect("/form-validation/index.jsp");
        } else {
            resp.getWriter().printf("<h1> Hello %s, welcome to our server!Your email is %s</h1>", username, email);
            resp.getWriter().printf("<a href=\"\">Please take me back!</a>");
        }

    }
}
