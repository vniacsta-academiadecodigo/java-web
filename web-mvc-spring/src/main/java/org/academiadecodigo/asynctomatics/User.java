package org.academiadecodigo.asynctomatics;

import org.springframework.web.bind.annotation.ModelAttribute;

public class User {

    private String name;
    private String email;
    private int phone;

    @ModelAttribute("name")
    public String getName() {
        return name;
    }

    @ModelAttribute("email")
    public String getEmail() {
        return email;
    }

    @ModelAttribute("phone")
    public int getPhone() {
        return phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }
}