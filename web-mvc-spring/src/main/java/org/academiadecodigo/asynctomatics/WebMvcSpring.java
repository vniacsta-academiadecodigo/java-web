package org.academiadecodigo.asynctomatics;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WebMvcSpring {

    private User user;

    public WebMvcSpring(User user) {
        this.user = user;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public ModelAndView display() {

        ModelAndView mv = new ModelAndView("index");
        mv.addObject("user", user);
        return mv;
    }
}
