<html>
<head>
    <title>Vania's Javabank</title>
    <style>
        body {
            text-align: center;
            background: darkslategrey;
            color: #fff;
            font-family: Cambria, serif;
        }

        div {
            width: fit-content;
            padding: 10px 30px;
            margin: 50px auto;
            border: solid 0.5px #fff;
            border-radius: 5px;
        }
    </style>
</head>
<body>
<div>
    <h1>JAVABANK - Customer #1</h1>
    <hr>
    <h2>${user.name}</h2>
    <p>Phone: ${user.phone}</p>
    <p>Email: ${user.email}</p>
</div>
</body>
</html>
